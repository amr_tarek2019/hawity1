<?php
return[
    'suggestions'=>'suggestions',
    'suggestionsdatatable'=>'suggestions datatable',
    'suggestionsdata'=>'suggestions data',
    'username'=>'user name',
    'useremail'=>'user email',
    'title'=>'title',
    'suggestion'=>'suggestion',
    'createdat'=>'created at',
    'actions'=>'actions',
    'show'=>'show',
    'delete'=>'delete',
    'showmessage'=>'show message',
    'back'=>'back'
];
