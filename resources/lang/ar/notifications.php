<?php
return[
    'notificationsdata'=>'بيانات الاشعارات',
    'notificationsdatatable'=>'جدول الاشعارات',
    'title'=>'عنوان',
    'notification'=>'اشعارات',
    'icon'=>'أيقونة',
    'createdat'=>'أنشئت في',
    'actions'=>'عمل',
    'submit'=>'خضع',
    'addnew'=>'اضف جديد',
    'selectuser'=>'اختار المستخدمين'
];