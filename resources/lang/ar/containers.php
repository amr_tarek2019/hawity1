<?php
return[
    'containerdatatable'=>'جدول بيانات الحاوية',
    'containersdata'=>'بيانات الحاويات',
    'addnew'=>'اضف جديد',
    'arabicname'=>'الاسم بالعربيه',
    'englishname'=>'الاسم بالانجليزيه',
    'company'=>'شركة',
    'price'=>'السعر',
    'distance'=>'بعد',
    'image'=>'صورة',
    'createdat'=>'أنشئت في',
    'updatedat'=>'تم التحديث في',
    'actions'=>'أفعال',
    'companyselect'=>'اختيار الشركة',
    'uploadfile'=>'رفع ملف',
    'textenglish'=>'نص الانجليزية',
    'textarabic'=>'النص العربيه',
    'submit'=>'خضع',
    'edit'=>'تعديل',
    'delete'=>'حذف',
    'editcontainer'=>'تعديل بيانات حاوية'
];
