<?php
return[
    'arabicabout'=>'العربيه',
    'englishabout'=>'الانجليزيه',
    'uploadimage'=>'ارفاق صورة',
    'image'=>'صورة',
    'submit'=>'خضع',
    'privacy'=>'الإجمالي',
    'arabicprivacy'=>'الخصوصيه العربيه',
    'englishprivacy'=>'الخصوصيه انجليزى',
    'about'=>'عن التطبيق',
    'settings'=>'الاعدادات'
];