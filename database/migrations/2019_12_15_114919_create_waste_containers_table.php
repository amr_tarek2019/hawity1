<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWasteContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_containers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_E');
            $table->string('name_A');
            $table->bigInteger('company_id')->unsigned();
            $table->string('price');
            $table->string('distance');
            $table->text('description_E');
            $table->text('description_A');
            $table->date('date');
            $table->string('month');
            $table->string('days');
            $table->string('total');
            $table->string('image');
            $table->timestamps();
            $table->foreign('company_id')
                ->references('id')->on('company_sellers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_containers');
    }
}
