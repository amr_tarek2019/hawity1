<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ar.notifications_title'=>'required',
            'en.notifications_title'=>'required',
            'ar.notifications_desc'=>'required',
            'en.notifications_desc'=>'required',
//            'notifications_img'=>''
        ];
    }
    public function messages(){
        return[
            'ar.notifications_title.required'=>'لم يتم ادخال عنوان التنبيه بالعربي ',
            'en.notifications_title.required'=>'لم يتم ادخال عنوان التنبيه بالانجليزية',
            'ar.notifications_desc.required'=>'لم يتم ادخال نص التنبيه بالعربي',
            'en.notifications_desc.required'=>'لم يتم ادخال نص التنبيه بالانجليزية',
        ];
    }
}
