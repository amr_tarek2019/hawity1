<?php

namespace App\Http\Controllers\API;

use App\AboutUs;
use App\Privacy;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;


class SettingsController extends BaseController
{
    public function indexAbout(Request $request)
    {
 $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $about=AboutUs::select('id','icons','about_'.$language. ' as about')->first();
        $response=[
            'message'=>'get data of about successfully',
            'status'=>202,
            'data'=>$about,
        ];
        return \Response::json($response,202);
    }

    public function indexPrivacy(Request $request)
    {
         $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $privacy=Privacy::select('id','icon','text_'.$language. ' as text')->first();
        $response=[
            'message'=>'get data of privacy successfully',
            'status'=>202,
            'data'=>$privacy,
        ];
        return \Response::json($response,202);
    }
}
