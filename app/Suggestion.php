<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $table='suggestions';
    protected $fillable=[ 'user_id','title', 'suggestion'];
    
      public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
