<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable=['user_id', 'icon', 'notification','title','created_at'];
    protected $table='notifications';
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAtAttribute()
    {
        return $this->attributes['created_at'];
    }
}
