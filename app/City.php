<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='cities';
    protected $fillable=['name_E','name_A'];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
