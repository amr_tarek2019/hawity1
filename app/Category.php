<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';
    protected $fillable=['name_E','name_A', 'image'];

        public function getImageAttribute($value)
    {
        if ($value) {
            return asset($value);
        } else {
            return asset('uploads/category/default.png');
        }
    }

    public function companySellers()
    {
        return $this->hasMany('App\CompanySeller');
    }
}
